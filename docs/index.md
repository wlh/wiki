# Notes

## Resources

## Islam

* [Worldwide Muslims Condemn List](https://docs.google.com/spreadsheets/d/1e8BjMW36CMNc4-qc9UNQku0blstZSzp5FMtkdlavqzc/htmlview) "When will Muslims condemn these atrocities."

### Gender equality

* [Delusions of Gender](https://www.goodreads.com/book/show/8031168-delusions-of-gender): accessible discussion of various societal biases.
* [Brain Storm](https://www.goodreads.com/book/show/8385283-brainstorm): a rigorous academic discussion the flaws in the study of sex differences.

### Drug Policy

* [Drugs without the hot air](https://www.goodreads.com/book/show/13592853-drugs-without-the-hot-air): An accessible, if typical, overview of considerations in approaching drug policy from a neurophyscopharmacologist.

* Pill Testing. https://ndarc.med.unsw.edu.au/node/301000974

### Gun Control

* https://www.abc.net.au/4corners/big-guns/10406306
* https://www.npr.org/2017/03/21/520953793/debate-over-silencers-hearing-protection-or-public-safety-threat

### Crime

* [Crime Statistics Victoria](https://www.crimestatistics.vic.gov.au/): The government agency responsible for collecting, analysing, and publishing crime statistics gathered from the Victoria Police.

### Reddit

* [MassTagger](https://masstagger.com/): Automated annotation of users that participate in 'problematic' subreddits.
* [Reddit Enhancement Suite](https://redditenhancementsuite.com/): Various tools for making life in Reddit easier. The ability to tag users with custom keywords is invaluable for labelling frequent posters who tend towards deliberate misinterpretation.


## Sexual Assault and Rape

* Sexual Coercion Attitudes among High School Students - Feltey, Ainslie & Geib
* A Meta-Analysis of Rape Education Programs - Brecklin & Forde
* Outcomes of an Intensive Program to Train Peer Facilitators for Campus Acquaintance Rape Education - Lonsway et al.
* An acquaintance rape education program for students transitioning to high school - Fay & Medway

## Arguments

### Gender Pay Gap

#### Average Pay

"Men work jobs with higher pay. If you compare the pay gap for the same job the difference is much smaller."

* This neglects an unexplained portion of the difference.
* This neglects considering the reasons genders gravitate towards specific fields.

#### Life Choices

"Women are innately drawn to lower paying employment."

* Correlation is not causation. Women being employed more commonly in lower paying employment does not mean they would naturally choose those careers.


[Women’s representation in science predicts national gender-science stereotypes: Evidence from 66 nations](http://dx.doi.org/10.1037/edu0000005).

> Even nations with high overall gender equity (e.g., the Netherlands) had strong gender-science stereotypes if men dominated science fields specifically. 
